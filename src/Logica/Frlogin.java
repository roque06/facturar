package Logica;

import Datos.Flogin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Frlogin {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "USUARIO", "CONTRASEÑA", "ESTADO", "ACCESO"};

        String[] registro = new String[5];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select *From login  where user like '%" + buscar + "%' order by id";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("id");
                registro[1] = rs.getString("user");
                registro[2] = rs.getString("pass");
                registro[3] = rs.getString("estado");
                registro[4] = rs.getString("acceso");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(Flogin dts) {
        sSQL = "insert into login (user,pass,estado,acceso)"
                + "VALUES(?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setString(1, dts.getUser());
            pst.setString(2, dts.getPass());
            pst.setString(3, dts.getEstado());
            pst.setString(4, dts.getAcceso());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(Flogin dts) {
        sSQL = "UPDATE login set user=?,pass=?,estado=?,acceso=?"
                + "where id=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setString(1, dts.getUser());
            pst.setString(2, dts.getPass());
            pst.setString(3, dts.getEstado());
            pst.setString(4, dts.getAcceso());

            pst.setInt(5, dts.getId());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(Flogin dts) {
        sSQL = "DELETE FROM login WHERE id=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public DefaultTableModel login(String login, String password) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "USUARIO", "CONTRASEÑA", "ESTADO", "ACCESO"};

        String[] registro = new String[5];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select *From login  where user= '" + login + "' and pass= '" + password + "' and estado='A'";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("id");
                registro[1] = rs.getString("user");
                registro[2] = rs.getString("pass");
                registro[3] = rs.getString("estado");
                registro[4] = rs.getString("acceso");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }
    }
}
