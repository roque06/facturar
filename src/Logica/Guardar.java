
package Logica;


import Datos.Fcliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;




public class Guardar  {
    
     private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID ", "Nombre", "Apellido", "Sexo","Cedula",};

        String[] registro = new String[5];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select *From cliente where nomcli like '%" + buscar + "%' order by idcli";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("idcli");
                registro[1] = rs.getString("nomcli");
                 registro[2] = rs.getString("apecli");
                registro[3] = rs.getString("sexo");
                 registro[4] = rs.getString("cedula");
            

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(Fcliente dts) {
        sSQL = "insert into cliente (nomcli,apecli,sexo,cedula)"
                + "VALUES(?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            
            pst.setString(1, dts.getNomcli());
            pst.setString(2, dts.getApecli());
            pst.setString(3, dts.getSexo());
             pst.setString(4, dts.getCedula());
        

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(Fcliente dts) {
        sSQL = "UPDATE cliente set nomcli=?,apecli=?,sexo=?,cedula=?"
                + "where idcli=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNomcli());
            pst.setString(2, dts.getApecli());
            pst.setString(3, dts.getSexo());
            pst.setString(4, dts.getCedula());
        
           

            pst.setInt(5, dts.getIdcli());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(Fcliente dts) {
        sSQL = "DELETE FROM cliente WHERE idcli=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdcli());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }
    
    
    
    
}