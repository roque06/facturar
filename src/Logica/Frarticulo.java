package Logica;

import Datos.Farticulos;
import Datos.Fcliente;
import formulario.factura;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Frarticulo {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = { "ID ARTICULO","CODIGO", "DESCRIPCION", "CANTIDAD", "COSTO", "PRECIO VENTA", "REORDEN"};

        String[] registro = new String[7];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select *From articulo where CONCAT (idarticulo,'',descripcion) like '%" + buscar + "%' order by codigo";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("idarticulo");
                registro[1] = rs.getString("codigo");
                registro[2] = rs.getString("descripcion");
                registro[3] = rs.getString("cantidad");
                registro[4] = rs.getString("costo");
                registro[5] = rs.getString("precio");
                registro[6] = rs.getString("reorden");
                
                

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(Farticulos dts) {
        sSQL = "insert into articulo (codigo,descripcion,cantidad,costo,precio,reorden)"
                + "VALUES(?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getCodigo());
            pst.setString(2, dts.getDescripcion());
            pst.setString(3, dts.getCantidad());
            pst.setString(4, dts.getCosto());
            pst.setString(5, dts.getPrecio());
            pst.setString(6, dts.getReorden());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(Farticulos dts) {
        sSQL = "UPDATE articulo set codigo=?,descripcion=?,cantidad=?,costo=?,precio=?,reorden=?"
                + "where idarticulo=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getCodigo());
            pst.setString(2, dts.getDescripcion());
            pst.setString(3, dts.getCantidad());
            pst.setString(4, dts.getCosto());
            pst.setString(5, dts.getPrecio());
            pst.setString(6, dts.getReorden());

            pst.setInt(7, dts.getIdarticulo());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(Farticulos dts) {
        sSQL = "DELETE FROM articulo WHERE idarticulo=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdarticulo());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }
}
