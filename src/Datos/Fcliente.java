
package Datos;


public class Fcliente {
   private int idcli;
   private String nomcli;
   private String apecli;
   private String sexo;
   private String cedula;
    public Fcliente() {
    }

    public Fcliente(int idcli, String nomcli, String apecli, String sexo, String cedula) {
        this.idcli = idcli;
        this.nomcli = nomcli;
        this.apecli = apecli;
        this.sexo = sexo;
        this.cedula = cedula;
    }

    public int getIdcli() {
        return idcli;
    }

    public void setIdcli(int idcli) {
        this.idcli = idcli;
    }

    public String getNomcli() {
        return nomcli;
    }

    public void setNomcli(String nomcli) {
        this.nomcli = nomcli;
    }

    public String getApecli() {
        return apecli;
    }

    public void setApecli(String apecli) {
        this.apecli = apecli;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    
    
    
    
}
