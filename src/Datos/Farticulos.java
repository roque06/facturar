
package Datos;


public class Farticulos {
    
    private int idarticulo;
    private String  codigo;
    
    private String descripcion;
    private String cantidad;
    private String costo;
    private String precio;
    private String reorden;

    public Farticulos() {
    }

    public Farticulos(int idarticulo, String codigo, String descripcion, String cantidad, String costo, String precio, String reorden) {
        this.idarticulo = idarticulo;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.costo = costo;
        this.precio = precio;
        this.reorden = reorden;
    }

    public int getIdarticulo() {
        return idarticulo;
    }

    public void setIdarticulo(int idarticulo) {
        this.idarticulo = idarticulo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getReorden() {
        return reorden;
    }

    public void setReorden(String reorden) {
        this.reorden = reorden;
    }

 
   
    
    
}
