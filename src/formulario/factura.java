package formulario;

import Datos.listaArticulos;
import Logica.Frarticulo;
import Logica.Guardar;
import Logica.conexion;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Admin
 */
public class factura extends javax.swing.JFrame {

    /**
     * Creates new form factura
     */
    public factura() {
        initComponents();

        NumeroFactura();
        txtbuscar.requestFocus();
        txtidiciente.setText("00000");
        txtcliente.setText("Cliente Generico");
        txtrnc.setText("N/A");
        txtncf.setText("N/A");

    }

    
    
    void detalleFactura (){
       for (int i= 0; i < tabla1.getRowCount(); i++){
           
           try {
               String sql2="";
               sql2= "INSERT INTO detalle_factura(num_factura,id_articulo,descripcion,cantidad,precio,itbis,importe)VALUES('"+txTfactura.getText()+"',"
               +"'"+tabla1.getValueAt(i, 0).toString()+"','"+tabla1.getValueAt(i, 1).toString()+"','"+tabla1.getValueAt(i, 2).toString()+"',"
               +"'"+tabla1.getValueAt(i, 3).toString()+"','"+tabla1.getValueAt(i, 4).toString()+"','"+tabla1.getValueAt(i, 5).toString()+"')";
          
                PreparedStatement psz2= cn.prepareStatement(sql2);
            int n;
            n= psz2.executeUpdate();
            
            
            
            
            
            
            
            
            
            
           } catch (Exception e) {
               JOptionPane.showConfirmDialog(null, e);
           }
       
        
       }
    
    
    
    JOptionPane.showMessageDialog(rootPane, "Factura Registrada");
     limpiar2();
     txtbuscar.requestFocus(true);
        
    
    }
    
   void imprimir(){
       DecimalFormat formateador = new DecimalFormat("###,###,###,###.##"); 
       ArrayList lista = new ArrayList ();
       
       JasperReport jr= null;
       
       for (int i= 0; i < tabla1.getRowCount(); i++){
        listaArticulos mortizar = new listaArticulos (tabla1.getValueAt(i, 2)+"",tabla1.getValueAt(i, 1)+"",
        tabla1.getValueAt(i, 3)+"",tabla1.getValueAt(i, 4)+"",tabla1.getValueAt(i, 5)+"");
        lista.add(mortizar);
       
       }
       
   
       
       try {
         jr= (JasperReport)JRLoader.loadObjectFromFile("reporteFactura.jasper");
         
         HashMap parametro = new HashMap ();
         
         parametro.put("empresa", "Empresa Factura");
         parametro.put("num_factura", txTfactura.getText());
         parametro.put("subtotal", txtsubtotal.getText());
         parametro.put("itebis", itebis_general.getText());
         parametro.put("total", txttotal.getText());
         parametro.put("idcliente",txtidiciente.getText());
         parametro.put("cliente", txtcliente.getText());
        
         
         JasperPrint jp= JasperFillManager.fillReport(jr, parametro, new JRBeanCollectionDataSource(lista));
         JasperViewer jv = new   JasperViewer (jp, false);
         jv.setVisible(true);
                 
         
           
           
           
           
           
           
           
           
       } catch (JRException ex) {
           ex.printStackTrace();
           JOptionPane.showMessageDialog(null,"ERROR\n" +ex.getMessage());
       }
      
       
}
    
    void cargar_filtro(String valor) {
        DefaultTableModel modelo2 = (DefaultTableModel) tablalistado.getModel();
        modelo2.getDataVector().clear();

        String[] registro = new String[10];

        String sql = " Select idarticulo,descripcion,cantidad,precio from articulo where CONCAT (idarticulo,'',descripcion) like '%" + valor + "%' ";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                registro[0] = rs.getString("idarticulo");

                registro[1] = rs.getString("descripcion");
                registro[2] = rs.getString("cantidad");

                registro[3] = rs.getString("precio");

                modelo2.addRow(registro);

            }

            tablalistado.setModel(modelo2);
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);

        }

    }

    void cargar_articulo(String valor) {

        String[] registro = new String[10];
        String sql = "select *From articulo where CONCAT (idarticulo,'',descripcion) like '%" + valor + "%'";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                registro[0] = rs.getString("idarticulo");

                registro[2] = rs.getString("descripcion");
                registro[3] = rs.getString("cantidad");

                registro[5] = rs.getString("precio");

                txtidarticulo.setText(registro[0]);
                txtdescripcion.setText(registro[2]);
                txtprecio.setText(registro[5]);
                txtcantidad.requestFocus();

            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);

        }

    }
    void limpiar2(){
        txtidiciente.setText("");
        txtcliente.setText("");
        
        txtbuscar.requestFocus(true);
    
    
    
    
    }

    void limpiar() {
        txtdescripcion.setText("");
        txtcantidad.setText("");
        txtidarticulo.setText("");
        txtprecio.setText("");
        txtbuscar.setText("");
        txtbuscar.requestFocus(true);

    }

    void llenar() {

        DecimalFormat formateador = new DecimalFormat("###,###,###,###,##");

        DefaultTableModel modelo2 = (DefaultTableModel) tabla1.getModel();
        String[] registros = new String[8];

        float precio2 = Float.parseFloat(txtprecio.getText());
        float cantidad2 = Float.parseFloat(txtcantidad.getText());
        float total2 = precio2 * cantidad2;
        float subtotal = total2 / 1.18f;

        registros[0] = txtidarticulo.getText();
        registros[1] = txtdescripcion.getText();
        registros[2] = txtcantidad.getText();
        registros[3] = txtprecio.getText();
        registros[4] = String.valueOf(total2 - subtotal);
        registros[5] = String.valueOf(subtotal);
        registros[6] = String.valueOf(total2);

        modelo2.addRow(registros);
        tabla1.setModel(modelo2);

    }

    void SumarProductos() {
        DecimalFormat formateador = new DecimalFormat("###,###,###,###.##");
        DecimalFormat formateador2 = new DecimalFormat("############.##");

        DefaultTableModel modelo2 = (DefaultTableModel) tabla1.getModel();
        float totalg = 0, totalg2 = 0;
        float itebisg = 0, itebisg2 = 0;
        float subtotalg = 0, subtotalg2 = 0;

        for (int i = 0; i < tabla1.getRowCount(); i++) {
            totalg = Float.parseFloat(tabla1.getValueAt(i, 6).toString());
            totalg2 = totalg + totalg2;

            itebisg = Float.parseFloat(tabla1.getValueAt(i, 4).toString());
            itebisg2 = itebisg + itebisg2;

            subtotalg = Float.parseFloat(tabla1.getValueAt(i, 5).toString());
            subtotalg2 = subtotalg + subtotalg2;

        }

        txttotal.setText(formateador.format(totalg2));
        itebis_general.setText(formateador.format(itebisg2));
        txtsubtotal.setText(formateador.format(subtotalg2));

        total_general = (formateador2.format(totalg2));
        itebis_general2 = (formateador2.format(itebisg2));
        subtotal = (formateador2.format(subtotalg2));
    }

    void NumeroFactura() {
        String id_factura = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT idfactura from contador");
            rq2.next();
            id_factura = rq2.getString("idfactura");

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

        int secuencia = Integer.parseInt(id_factura);
        secuencia = secuencia + 1;
        txTfactura.setText("PC1-" + String.valueOf(secuencia));

    }
    
    void GuardarFactura (){
    
        try {
           String sql2="";
           sql2= "INSERT INTO factura(num_factura,tipo_factura,fecha,sub_total,itbis,total,id_cliente,nom_cliente )"
                   + "VALUES('" + txTfactura.getText() +"','"+ jComboBox1.getSelectedItem()+ "',now(),'" + subtotal +"','"+ itebis_general2+"',"
                   + "'" +total_general+ "','" + txtidiciente.getText()+"','"+ txtcliente.getText()+"')";
                         
                           
            PreparedStatement psz2= cn.prepareStatement(sql2);
            int n;
            n= psz2.executeUpdate();
            
            if (n>0){
            }
            
            
            
            
        } catch (Exception e) {
        JOptionPane.showConfirmDialog(null, e);
        
    }
    
    
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtrnc = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtncf = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtbuscar = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txTfactura = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btnimpimir2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        txtcliente = new javax.swing.JTextField();
        txtidiciente = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablalistado = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtidarticulo = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtdescripcion = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtcantidad = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtprecio = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabla1 = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();
        txtsubtotal = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        itebis_general = new javax.swing.JTextField();
        txttotal = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        btnguardar = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox<>();
        btnimpimir1 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("GENERAR FACTURA");

        jPanel1.setBackground(new java.awt.Color(0, 204, 204));
        jPanel1.setForeground(new java.awt.Color(0, 204, 204));

        jLabel1.setText("ID CLIENTE");

        jLabel2.setText("CLIENTE");

        txtrnc.setEditable(false);
        txtrnc.setCaretColor(new java.awt.Color(0, 255, 255));

        jLabel3.setText("RNC");

        txtncf.setEditable(false);
        txtncf.setCaretColor(new java.awt.Color(0, 255, 255));

        jLabel4.setText("NCF");

        txtbuscar.setBackground(new java.awt.Color(102, 102, 102));
        txtbuscar.setForeground(new java.awt.Color(255, 255, 255));
        txtbuscar.setCaretColor(new java.awt.Color(0, 255, 255));
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        jLabel5.setText("BUCAR");

        txTfactura.setEditable(false);
        txTfactura.setCaretColor(new java.awt.Color(0, 255, 255));
        txTfactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txTfacturaActionPerformed(evt);
            }
        });

        jLabel6.setText("FACTURA#");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 487, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 177, Short.MAX_VALUE)
        );

        btnimpimir2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/files/reportes.jpg"))); // NOI18N
        btnimpimir2.setText("Imprimir  ");
        btnimpimir2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimpimir2ActionPerformed(evt);
            }
        });

        jButton1.setText("Buscar Cliente");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel1)
                                        .addComponent(jLabel6)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING))
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(15, 15, 15))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(txtrnc, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtbuscar)
                            .addComponent(txTfactura, javax.swing.GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)))
                    .addComponent(txtidiciente, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtcliente, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtncf, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(70, 70, 70)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(217, 217, 217))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(464, 464, 464)
                    .addComponent(btnimpimir2)
                    .addContainerGap(465, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txTfactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtidiciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtrnc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtncf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(441, 441, 441))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(299, 299, 299)
                    .addComponent(btnimpimir2)
                    .addContainerGap(299, Short.MAX_VALUE)))
        );

        jPanel4.setBackground(new java.awt.Color(0, 153, 153));
        jPanel4.setForeground(new java.awt.Color(102, 102, 102));

        tablalistado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID ARTICULO", "DESCRIPCION", "CANTIDAD", "PRECIO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablalistado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablalistadoMouseClicked(evt);
            }
        });
        tablalistado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablalistadoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablalistadoKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(tablalistado);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 571, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(0, 102, 102));

        jLabel7.setBackground(new java.awt.Color(0, 153, 153));
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("ID ARTICULO");

        txtidarticulo.setBackground(new java.awt.Color(102, 102, 102));
        txtidarticulo.setForeground(new java.awt.Color(255, 255, 255));

        jLabel8.setBackground(new java.awt.Color(0, 153, 153));
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("DESCRIPCION");

        txtdescripcion.setBackground(new java.awt.Color(102, 102, 102));
        txtdescripcion.setForeground(new java.awt.Color(255, 255, 255));

        jLabel9.setBackground(new java.awt.Color(0, 153, 153));
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("CANTIDAD");

        txtcantidad.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtcantidadMousePressed(evt);
            }
        });
        txtcantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcantidadActionPerformed(evt);
            }
        });

        jLabel10.setBackground(new java.awt.Color(0, 153, 153));
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("PRECIO UNIDAD");

        tabla1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID ARTICULO", "DESCRIPCION", "CANTIDAD", "PRECIO UNIDA", "ITEBIS", "IMPORTE", "TOTAL"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabla1.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(tabla1);
        if (tabla1.getColumnModel().getColumnCount() > 0) {
            tabla1.getColumnModel().getColumn(0).setMinWidth(100);
            tabla1.getColumnModel().getColumn(0).setMaxWidth(100);
            tabla1.getColumnModel().getColumn(1).setMinWidth(360);
            tabla1.getColumnModel().getColumn(1).setMaxWidth(360);
            tabla1.getColumnModel().getColumn(2).setMinWidth(80);
            tabla1.getColumnModel().getColumn(2).setMaxWidth(80);
            tabla1.getColumnModel().getColumn(3).setMinWidth(80);
            tabla1.getColumnModel().getColumn(3).setMaxWidth(80);
            tabla1.getColumnModel().getColumn(4).setMinWidth(80);
            tabla1.getColumnModel().getColumn(4).setMaxWidth(80);
            tabla1.getColumnModel().getColumn(5).setMinWidth(100);
            tabla1.getColumnModel().getColumn(5).setMaxWidth(100);
            tabla1.getColumnModel().getColumn(6).setMinWidth(100);
            tabla1.getColumnModel().getColumn(6).setMaxWidth(100);
        }

        jLabel11.setBackground(new java.awt.Color(0, 153, 153));
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("SUB-TOTAL");

        jLabel12.setBackground(new java.awt.Color(0, 153, 153));
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("ITEBIS");

        jLabel13.setBackground(new java.awt.Color(0, 153, 153));
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("TOTAL");

        btnguardar.setBackground(new java.awt.Color(255, 255, 255));
        btnguardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/guardar.jpg"))); // NOI18N
        btnguardar.setText("Guardar Factura");
        btnguardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnguardarActionPerformed(evt);
            }
        });

        jComboBox1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "CONTADO", "CREDITO" }));

        btnimpimir1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/files/reportes.jpg"))); // NOI18N
        btnimpimir1.setText("Imprimir  ");
        btnimpimir1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimpimir1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(276, 276, 276)
                        .addComponent(btnimpimir1)
                        .addGap(23, 23, 23)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addComponent(btnguardar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel11))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txttotal, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                            .addComponent(txtsubtotal)
                            .addComponent(itebis_general)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtidarticulo, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtdescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtprecio, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtcantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 901, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtidarticulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(txtdescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(txtprecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(txtcantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(txtsubtotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(itebis_general, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnguardar)
                                .addComponent(btnimpimir1))
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(0, 153, 153));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablalistadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablalistadoMouseClicked
        int fila = this.tablalistado.getSelectedRow();

        txtidarticulo.setText(tablalistado.getValueAt(fila, 0).toString());
        txtdescripcion.setText(tablalistado.getValueAt(fila, 1).toString());

        txtprecio.setText(tablalistado.getValueAt(fila, 3).toString());

        txtcantidad.requestFocus();

        // TODO add your handling code here:
    }//GEN-LAST:event_tablalistadoMouseClicked

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarKeyPressed
    TableRowSorter trs;
    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        cargar_filtro(txtbuscar.getText());
// TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void tablalistadoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablalistadoKeyReleased

        // TODO add your handling code here:
    }//GEN-LAST:event_tablalistadoKeyReleased

    private void tablalistadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablalistadoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tablalistadoKeyPressed

    private void txtcantidadMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtcantidadMousePressed

        // TODO add your handling code here:
    }//GEN-LAST:event_txtcantidadMousePressed

    private void txtcantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcantidadActionPerformed
        if (txtcantidad.getText().length() == 0) {
            JOptionPane.showConfirmDialog(rootPane, "Debes Ingresar la Cantidad");
            txtcantidad.requestFocus();
            return;
        }

        llenar();
        SumarProductos();
        limpiar();

// TODO add your handling code here:
    }//GEN-LAST:event_txtcantidadActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        cliente form = new cliente();
        form.toFront();
        form.setVisible(true);        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txTfacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txTfacturaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txTfacturaActionPerformed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        cargar_articulo(txtbuscar.getText());        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarActionPerformed

    private void btnguardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnguardarActionPerformed
           GuardarFactura ();
           detalleFactura();
           
           try {
             PreparedStatement psU= cn.prepareStatement("Update contador set idfactura= idfactura+1");
             psU.executeUpdate();
  
        } catch (Exception e) {
               
      JOptionPane.showConfirmDialog(rootPane, e);
      
        }
       this.dispose();
         new factura ().setVisible(true);   

        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarActionPerformed

    private void btnimpimir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimpimir1ActionPerformed
imprimir();        
    }//GEN-LAST:event_btnimpimir1ActionPerformed

    private void btnimpimir2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimpimir2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnimpimir2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(factura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(factura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(factura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(factura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new factura().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnguardar;
    private javax.swing.JButton btnimpimir1;
    private javax.swing.JButton btnimpimir2;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JTextField itebis_general;
    public javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    public static javax.swing.JPanel jPanel1;
    public static javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable tabla1;
    private javax.swing.JTable tablalistado;
    private javax.swing.JTextField txTfactura;
    public static javax.swing.JTextField txtbuscar;
    private javax.swing.JTextField txtcantidad;
    public static javax.swing.JTextField txtcliente;
    private javax.swing.JTextField txtdescripcion;
    public static javax.swing.JTextField txtidarticulo;
    public static javax.swing.JTextField txtidiciente;
    public static javax.swing.JTextField txtncf;
    private javax.swing.JTextField txtprecio;
    public static javax.swing.JTextField txtrnc;
    private javax.swing.JTextField txtsubtotal;
    private javax.swing.JTextField txttotal;
    // End of variables declaration//GEN-END:variables

    conexion cc = new conexion();
    Connection cn = cc.conectar();

    String tipo_factura = "";
    String itebis_general2 = "";
    String subtotal = "";
    String total_general = "";

}
