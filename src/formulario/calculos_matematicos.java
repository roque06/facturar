
package formulario;
//LIBREIRAS

import java.awt.Color;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class calculos_matematicos extends javax.swing.JFrame {
    
//VARIABLES PUBLICAS
    String op="";
   
    public calculos_matematicos() {
        initComponents();
    sumar.setSelected(true);
num1.requestFocus(true);
    }
    
    void limpiar_datos(){
      num1.setText("");
      num2.setText(""); 
      total.setText("");
      sumar.setSelected(true);
      num1.requestFocus(true);
      
      
      
    }
    
    void sumar_numero(){
        float a=Float.parseFloat(num1.getText());
float b=Float.parseFloat(num2.getText());
float resultado=a+b;
total.setText(String.valueOf(resultado));
        
    }
    void multiplicar_numero(){
        float a=Float.parseFloat(num1.getText());
float b=Float.parseFloat(num2.getText());
float resultado=a*b;
total.setText(String.valueOf(resultado));
        
    }
    void restar_numero(){
        float a=Float.parseFloat(num1.getText());
float b=Float.parseFloat(num2.getText());
float resultado=a-b;
total.setText(String.valueOf(resultado));
        
    }
    void dividir_numero(){
        float a=Float.parseFloat(num1.getText());
float b=Float.parseFloat(num2.getText());
float resultado=a/b;
total.setText(String.valueOf(resultado));
        
    }
    void agregar_datos(){
    DefaultTableModel modelo2 = (DefaultTableModel)t_numero.getModel();
    String [] registros = new String[4];
    
    
    registros[0]=num1.getText();
    registros[1]=num2.getText();
    registros[2]=op;
    registros[3]=total.getText();
    modelo2.addRow(registros);
    t_numero.setModel(modelo2);
    
    }
    //CLASES Y FUNCIONES
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        total = new javax.swing.JTextField();
        salir = new javax.swing.JButton();
        procesar = new javax.swing.JButton();
        num2 = new javax.swing.JTextField();
        num1 = new javax.swing.JTextField();
        sumar = new javax.swing.JRadioButton();
        multiplicar = new javax.swing.JRadioButton();
        restar = new javax.swing.JRadioButton();
        dividir = new javax.swing.JRadioButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        t_numero = new javax.swing.JTable();
        limpiar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("CALCULOS MATEMATICOS");

        jPanel1.setBackground(new java.awt.Color(0, 153, 153));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("CALCULOS MATEMATICOS"));

        jLabel1.setText("NUMERO1");

        jLabel2.setText("NUMERO2");

        jLabel3.setText("TOTAL");

        salir.setBackground(new java.awt.Color(153, 153, 153));
        salir.setText("SALIR");
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });

        procesar.setBackground(new java.awt.Color(153, 153, 153));
        procesar.setText("PROCESAR");
        procesar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                procesarActionPerformed(evt);
            }
        });

        num2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                num2ActionPerformed(evt);
            }
        });

        num1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                num1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(sumar);
        sumar.setText("SUMA");
        sumar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sumarActionPerformed(evt);
            }
        });

        buttonGroup1.add(multiplicar);
        multiplicar.setText("MULTIPLICACION");
        multiplicar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                multiplicarActionPerformed(evt);
            }
        });

        buttonGroup1.add(restar);
        restar.setText("RESTA");
        restar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                restarActionPerformed(evt);
            }
        });

        buttonGroup1.add(dividir);
        dividir.setText("DIVICION");
        dividir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dividirActionPerformed(evt);
            }
        });

        t_numero.setBackground(new java.awt.Color(102, 102, 102));
        t_numero.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NUMERO1", "NUMERO2", "OPERACION", "TOTAL"
            }
        ));
        t_numero.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                t_numeroMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(t_numero);

        limpiar.setBackground(new java.awt.Color(153, 153, 153));
        limpiar.setText("LIMPIAR");
        limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limpiarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(58, 58, 58)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(num2, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(num1, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(total, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(sumar, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(14, 14, 14)
                                .addComponent(restar))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(salir)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(procesar)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(limpiar))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(dividir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(multiplicar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(317, Short.MAX_VALUE))
            .addComponent(jScrollPane1)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(num1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(sumar)
                        .addComponent(restar)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(num2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(multiplicar)
                        .addComponent(dividir)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(total, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(procesar)
                    .addComponent(salir)
                    .addComponent(limpiar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void num1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_num1ActionPerformed
if (num1.getText().equals("")){
            JOptionPane.showMessageDialog(null,"EL NUMERO 1 ESTA EN BLANCO");
            num1.requestFocus(true);
            return;
        }
        
        num2.requestFocus(true);
    num2.setBackground(Color.GRAY);
    }//GEN-LAST:event_num1ActionPerformed

    private void procesarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_procesarActionPerformed
        if (num1.getText().equals("")){
            JOptionPane.showMessageDialog(null,"EL NUMERO 1 ESTA EN BLANCO");
            num1.requestFocus(true);
            return;
        }
        if (num2.getText().equals("")){
            JOptionPane.showMessageDialog(null,"EL NUMERO 2 ESTA EN BLANCO");
            num2.requestFocus(true);
            return;
        }
        if(sumar.isSelected()){
        sumar_numero();
        op="SUMA";
        }
        if(multiplicar.isSelected()){
        multiplicar_numero();
        op="MULTIPLICACION";
        }
        if(restar.isSelected()){
        restar_numero();
        op="RESTA";
        }
        if(dividir.isSelected()){
        dividir_numero();
        op="DIVICION";
        }
        agregar_datos();
    }//GEN-LAST:event_procesarActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        this.dispose();
    }//GEN-LAST:event_salirActionPerformed

    private void num2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_num2ActionPerformed
    if (num1.getText().equals("")){
            JOptionPane.showMessageDialog(null,"EL NUMERO 1 ESTA EN BLANCO");
            num1.requestFocus(true);
            return;
        }
    
        if (num2.getText().equals("")){
            JOptionPane.showMessageDialog(null,"EL NUMERO 2 ESTA EN BLANCO");
            num2.requestFocus(true);
            return;
        } 
   if(sumar.isSelected()){
        sumar_numero();
        op="SUMA";
        }
        if(multiplicar.isSelected()){
        multiplicar_numero();
        op="MULTIPLICACION";
        }
        if(restar.isSelected()){
        restar_numero();
        op="RESTA";
        }
        if(dividir.isSelected()){
        dividir_numero();
        op="DIVICION";
        }
        agregar_datos();
    }//GEN-LAST:event_num2ActionPerformed

    private void sumarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sumarActionPerformed
    procesar.doClick();
    }//GEN-LAST:event_sumarActionPerformed

    private void dividirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dividirActionPerformed
    procesar.doClick();   
    }//GEN-LAST:event_dividirActionPerformed

    private void restarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_restarActionPerformed
    procesar.doClick();   
    }//GEN-LAST:event_restarActionPerformed

    private void multiplicarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_multiplicarActionPerformed
    procesar.doClick();    
    }//GEN-LAST:event_multiplicarActionPerformed

    private void limpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limpiarActionPerformed
    limpiar_datos();        
    }//GEN-LAST:event_limpiarActionPerformed

    private void t_numeroMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_t_numeroMouseClicked
    int fila = t_numero.getSelectedRow();
    if(fila>=0){
    num1.setText(t_numero.getValueAt(fila,0).toString());
    num2.setText(t_numero.getValueAt(fila,1).toString());
    total.setText(t_numero.getValueAt(fila,3).toString());
    op=(t_numero.getValueAt(fila,2).toString());
    if(op.equals("SUMA")){
      sumar.setSelected(true);  
    }
    if(op.equals("MULTIPLICACION")){
      multiplicar.setSelected(true);  
    }
    if(op.equals("RESTA")){
      restar.setSelected(true);  
    }
    if(op.equals("DIVISION")){
      dividir.setSelected(true);  
    }
    }
    }//GEN-LAST:event_t_numeroMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(calculos_matematicos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(calculos_matematicos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(calculos_matematicos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(calculos_matematicos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new calculos_matematicos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JRadioButton dividir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton limpiar;
    private javax.swing.JRadioButton multiplicar;
    private javax.swing.JTextField num1;
    private javax.swing.JTextField num2;
    private javax.swing.JButton procesar;
    private javax.swing.JRadioButton restar;
    private javax.swing.JButton salir;
    private javax.swing.JRadioButton sumar;
    private javax.swing.JTable t_numero;
    private javax.swing.JTextField total;
    // End of variables declaration//GEN-END:variables
}
