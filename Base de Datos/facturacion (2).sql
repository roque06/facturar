-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 30-08-2020 a las 03:43:47
-- Versión del servidor: 8.0.17
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `facturacion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE `articulo` (
  `idarticulo` int(10) NOT NULL,
  `codigo` int(11) NOT NULL,
  `descripcion` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `cantidad` float(12,2) NOT NULL,
  `costo` float(12,2) NOT NULL,
  `precio` float(12,2) NOT NULL,
  `reorden` float(12,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`idarticulo`, `codigo`, `descripcion`, `cantidad`, `costo`, `precio`, `reorden`) VALUES
(1, 1008, 'Lata de Salsa Baldon', 50.00, 150.00, 175.00, 100.00),
(2, 123, 'Lata guandules baldon', 34.00, 55.00, 60.00, 70.00),
(3, 1009, 'COCA COLA ', 15.00, 25.00, 30.00, 30.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idcli` int(5) NOT NULL,
  `nomcli` varchar(100) NOT NULL,
  `apecli` varchar(100) NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `cedula` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idcli`, `nomcli`, `apecli`, `sexo`, `cedula`) VALUES
(3, 'JUAN ', 'PEREZ', 'm', '4548848254'),
(4, 'CLIENTE GENERICO', 'PERSONA', 'm', 'N/A'),
(5, 'CARLOS DANIEL', 'DELGADO ABREU', 'M', '4845487545');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contador`
--

CREATE TABLE `contador` (
  `idfactura` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `contador`
--

INSERT INTO `contador` (`idfactura`) VALUES
(19);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_factura`
--

CREATE TABLE `detalle_factura` (
  `num_factura` varchar(50) NOT NULL,
  `id_articulo` varchar(50) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `cantidad` varchar(50) NOT NULL,
  `precio` varchar(50) NOT NULL,
  `itbis` varchar(50) NOT NULL,
  `importe` varchar(50) NOT NULL,
  `registro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `detalle_factura`
--

INSERT INTO `detalle_factura` (`num_factura`, `id_articulo`, `descripcion`, `cantidad`, `precio`, `itbis`, `importe`, `registro`) VALUES
('PC1-14', '3', 'COCA COLA ', '2', '30.00', '9.152538', '50.84746', 1),
('PC1-15', '2', 'Lata guandules baldon', '3', '60.00', '27.457626', '152.54237', 2),
('PC1-15', '1', 'Lata de Salsa Baldon', '2', '175.00', '53.38983', '296.61017', 3),
('PC1-15', '3', 'COCA COLA ', '12', '30.00', '54.915253', '305.08475', 4),
('PC1-15', '2', 'Lata guandules baldon', '3', '60.00', '27.457626', '152.54237', 5),
('PC1-15', '1', 'Lata de Salsa Baldon', '10', '175.00', '266.9491', '1483.0509', 6),
('PC1-15', '2', 'Lata guandules baldon', '3', '60.00', '27.457626', '152.54237', 7),
('PC1-15', '1', 'Lata de Salsa Baldon', '15', '175.00', '400.42358', '2224.5764', 8),
('PC1-15', '1', 'Lata de Salsa Baldon', '3', '175.00', '80.08472', '444.91528', 9),
('PC1-15', '2', 'Lata guandules baldon', '3', '60.00', '27.457626', '152.54237', 10),
('PC1-15', '2', 'Lata guandules baldon', '2', '60.00', '18.305077', '101.69492', 11),
('PC1-15', '1', 'Lata de Salsa Baldon', '3', '175.00', '80.08472', '444.91528', 12),
('PC1-15', '2', 'Lata guandules baldon', '3', '60.00', '27.457626', '152.54237', 13),
('PC1-15', '3', 'COCA COLA ', '2', '30.00', '9.152538', '50.84746', 14),
('PC1-16', '3', 'COCA COLA ', '2', '30.00', '9.152538', '50.84746', 15),
('PC1-16', '1', 'Lata de Salsa Baldon', '2', '175.00', '53.38983', '296.61017', 16),
('PC1-16', '3', 'COCA COLA ', '1', '30.00', '4.576269', '25.42373', 17),
('PC1-16', '2', 'Lata guandules baldon', '1', '60.00', '9.152538', '50.84746', 18),
('PC1-16', '2', 'Lata guandules baldon', '3', '60.00', '27.457626', '152.54237', 19),
('PC1-16', '1', 'Lata de Salsa Baldon', '12', '175.00', '320.33887', '1779.6611', 20),
('PC1-16', '2', 'Lata guandules baldon', '3', '60.00', '27.457626', '152.54237', 21),
('PC1-16', '1', 'Lata de Salsa Baldon', '2', '175.00', '53.38983', '296.61017', 22),
('PC1-17', '2', 'Lata guandules baldon', '3', '60.00', '27.457626', '152.54237', 23),
('PC1-18', '2', 'Lata guandules baldon', '3', '60.00', '27.457626', '152.54237', 24),
('PC1-18', '1', 'Lata de Salsa Baldon', '1', '175.00', '26.694916', '148.30508', 25),
('PC1-18', '2', 'Lata guandules baldon', '12', '60.00', '109.830505', '610.1695', 26),
('PC1-18', '1', 'Lata de Salsa Baldon', '3', '175.00', '80.08472', '444.91528', 27),
('PC1-18', '1', 'Lata de Salsa Baldon', '2', '175.00', '53.38983', '296.61017', 28),
('PC1-18', '3', 'COCA COLA ', '1', '30.00', '4.576269', '25.42373', 29),
('PC1-19', '1', 'Lata de Salsa Baldon', '2', '175.00', '53.38983', '296.61017', 30),
('PC1-19', '2', 'Lata guandules baldon', '3', '60.00', '27.457626', '152.54237', 31),
('PC1-19', '3', 'COCA COLA ', '3', '30.00', '13.728813', '76.27119', 32),
('PC1-19', '1', 'Lata de Salsa Baldon', '2', '175.00', '53.38983', '296.61017', 33),
('PC1-19', '3', 'COCA COLA ', '2', '30.00', '9.152538', '50.84746', 34),
('PC1-19', '1', 'Lata de Salsa Baldon', '2', '175.00', '53.38983', '296.61017', 35),
('PC1-19', '3', 'COCA COLA ', '2', '30.00', '9.152538', '50.84746', 36),
('PC1-19', '1', 'Lata de Salsa Baldon', '2', '175.00', '53.38983', '296.61017', 37),
('PC1-19', '3', 'COCA COLA ', '2', '30.00', '9.152538', '50.84746', 38),
('PC1-19', '3', 'COCA COLA ', '3', '30.00', '13.728813', '76.27119', 39),
('PC1-19', '3', 'COCA COLA ', '3', '30.00', '13.728813', '76.27119', 40),
('PC1-19', '1', 'Lata de Salsa Baldon', '3', '175.00', '80.08472', '444.91528', 41),
('PC1-19', '2', 'Lata guandules baldon', '3', '60.00', '27.457626', '152.54237', 42),
('PC1-19', '1', 'Lata de Salsa Baldon', '2', '175.00', '53.38983', '296.61017', 43);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `num_factura` varchar(50) NOT NULL,
  `tipo_factura` varchar(20) NOT NULL,
  `fecha` date NOT NULL,
  `sub_total` varchar(50) NOT NULL,
  `itbis` varchar(50) NOT NULL,
  `total` varchar(50) NOT NULL,
  `id_cliente` varchar(20) NOT NULL,
  `nom_cliente` varchar(50) NOT NULL,
  `registro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`num_factura`, `tipo_factura`, `fecha`, `sub_total`, `itbis`, `total`, `id_cliente`, `nom_cliente`, `registro`) VALUES
('PC1-7', 'CONTADO', '2020-08-24', '6504,24', '1170,76', '7675', '', '', 1),
('PC1-8', 'CONTADO', '2020-08-29', '1152,54', '207,46', '1360', '00000', 'Cliente Generico', 2),
('PC1-9', 'CONTADO', '2020-08-29', '50,85', '9,15', '60', '00000', 'Cliente Generico', 3),
('PC1-10', 'CONTADO', '2020-08-29', '152,54', '27,46', '180', '00000', 'Cliente Generico', 4),
('PC1-11', 'CONTADO', '2020-08-29', '152,54', '27,46', '180', '00000', 'Cliente Generico', 5),
('PC1-12', 'CONTADO', '2020-08-29', '152,54', '27,46', '180', '00000', 'Cliente Generico', 6),
('PC1-13', 'CONTADO', '2020-08-29', '50,85', '9,15', '60', '5', 'CARLOS DANIEL', 7),
('PC1-14', 'CONTADO', '2020-08-29', '50,85', '9,15', '60', '00000', 'Cliente Generico', 8),
('PC1-15', 'CONTADO', '2020-08-29', '6114,41', '1100,59', '7215', '00000', 'Cliente Generico', 9),
('PC1-16', 'CONTADO', '2020-08-29', '2805,08', '504,92', '3310', '00000', 'Cliente Generico', 10),
('PC1-17', 'CONTADO', '2020-08-29', '152,54', '27,46', '180', '00000', 'Cliente Generico', 11),
('PC1-18', 'CONTADO', '2020-08-29', '1677,97', '302,03', '1980', '00000', 'Cliente Generico', 12),
('PC1-19', 'CONTADO', '2020-08-29', '2614,41', '470,59', '3085', '5', 'CARLOS DANIEL', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grado`
--

CREATE TABLE `grado` (
  `idgrado` int(11) NOT NULL,
  `Nivel` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `grado`
--

INSERT INTO `grado` (`idgrado`, `Nivel`) VALUES
(1, 'roque');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `user` varchar(20) NOT NULL,
  `pass` varchar(20) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `acceso` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`id`, `user`, `pass`, `estado`, `acceso`) VALUES
(1, 'roque ', '123', 'A', 'Administrador'),
(2, 'root', '123', 'A', 'Administrador');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`idarticulo`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcli`);

--
-- Indices de la tabla `detalle_factura`
--
ALTER TABLE `detalle_factura`
  ADD PRIMARY KEY (`registro`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`registro`);

--
-- Indices de la tabla `grado`
--
ALTER TABLE `grado`
  ADD PRIMARY KEY (`idgrado`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulo`
--
ALTER TABLE `articulo`
  MODIFY `idarticulo` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcli` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `detalle_factura`
--
ALTER TABLE `detalle_factura`
  MODIFY `registro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `registro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `grado`
--
ALTER TABLE `grado`
  MODIFY `idgrado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
